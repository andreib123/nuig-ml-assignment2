import numpy as np

def relu(x):
    return np.maximum(x, 0)


def deriv_relu(x):
    dims = x.shape
    deriv_x = np.zeros_like(x)  # empty matrix of same dims as x
    for i in range(dims[1] - 1):  # loop through columns
        for j in range(dims[0] - 1):  # loop through rows
            if x[j, i] < 0:
                deriv_x[j, i] = 0
            else:
                deriv_x[j, i] = 1
    return deriv_x


def softmax(M):
    expM = np.exp(M - np.max(M))
    res = expM / expM.sum(axis=0)  # softmax vals for each column
    return res


def cost_function(Y_HAT, Y):
    cost = -np.mean(np.multiply(Y, np.log(Y_HAT)))  # cross entropy loss function used
    return cost


def calculate_accuracy(Y_HAT, Y, threshold):
    y_hat_th = (Y_HAT > threshold).astype(int)  # any value > threshold in A2 set to 1, otherwise 0
    correct_classifs_mx = np.multiply(Y, y_hat_th)
    correct_classifs = correct_classifs_mx.sum()  # number of correctly classified hazelnuts
    number_training_ex = Y.shape[1]
    epoch_accuracy_perc = (correct_classifs / number_training_ex) * 100  # percentage of correctly classified hazelnuts
    return epoch_accuracy_perc


class MLP:

    def __init__(self, learning_rate, inputs_neurons, hidden_neurons, output_neurons):
        self.inputs_neurons = inputs_neurons  # number of input neurons
        self.hidden_neurons = hidden_neurons  # number of hidden neurons
        self.output_neurons = output_neurons  # number of output neurons

        np.random.seed(0)
        self.W1 = np.random.randn(hidden_neurons, inputs_neurons) * 0.01  # input layer to hidden layer weights matrix
        self.W2 = np.random.randn(output_neurons, hidden_neurons) * 0.01  # hidden layer to output layer weights matrix
        self.b1 = np.random.randn(hidden_neurons, 1) * 0.01  # hidden layer biases matrix
        self.b2 = np.random.randn(output_neurons, 1) * 0.01  # output layer biases matrix
        self.learning_rate = learning_rate

    def forward_prop(self, X):
        Z1 = np.dot(self.W1, X) + self.b1
        A1 = relu(Z1)
        Z2 = np.dot(self.W2, A1) + self.b2
        A2 = softmax(Z2)
        return Z1, A1, Z2, A2

    # calculate gradients
    def back_prop(self, Z1, A1, A2, X, Y):
        one_div_m = 1.0 / X.shape[1]
        dZ2 = A2 - Y
        dW2 = one_div_m * np.dot(dZ2, A1.T)
        db2 = one_div_m * np.sum(dZ2, axis=1)
        dZ1 = np.multiply(np.dot(self.W2.T, dZ2), deriv_relu(Z1))
        dW1 = one_div_m * np.dot(dZ1, X.T)
        db1 = one_div_m * np.sum(dZ1, axis=1)
        return dW1, db1, dW2, db2

    def update_weights(self, dW1, db1, dW2, db2):
        self.W1 = self.W1 - self.learning_rate * dW1
        self.b1 = self.b1 - self.learning_rate * db1
        self.W2 = self.W2 - self.learning_rate * dW2
        self.b2 = self.b2 - self.learning_rate * db2
