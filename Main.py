import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import confusion_matrix
from random import randrange
from MLP import *

"""
    Writes to a text file results from cross validation:
        - the confusion matrices
        - the accuracies
        - overall accuracy

    Parameters
    ----------
    conf_mx_arr : ndarray
        Array of confusion matrices.
    acc_arr : ndarray
       Array of accuracy values.  
"""


def write_conf_file(conf_mx_arr, acc_arr):
    with open('crossvalidout.txt', 'w') as o_file:
        i = 1
        for data_slice in conf_mx_arr:
            o_file.write('\n# Confusion matrix {0}\n'.format(i))
            np.savetxt(o_file, data_slice, fmt='%-7.2f')
            i += 1
        i = 1
        for val in acc_arr:
            o_file.write('\n# Accuracy for test set {0}: '.format(i))
            np.savetxt(o_file, [val], fmt='%-7.2f')
            i += 1
        o_file.write('\n# Average accuracy: ')
        np.savetxt(o_file, [np.average(acc_arr)], fmt='%-7.2f')


"""
    Creates training and testing dataframes for Weka support

    Parameters
    ----------
    df : Dataframe
        Dataframe of entire dataset.
        
    Returns
    -------
    training : Pandas Dataframe
        training dataset (unnormalised)
        
    testing : Pandas Dataframe
        testing dataset (unnormalised)
"""


def preprocess_weka_datasets(df):
    df_avellana = df.loc[df['avellana'] == 1]  # dataframe of avellana hazelnuts (approx. 1/3 of overall dataset)
    df_americana = df.loc[df['americana'] == 1]
    df_cornuta = df.loc[df['cornuta'] == 1]

    avellana_testing = df_avellana.sample(
        frac=0.33)  # random select 1/3 of avellana dataset (approx. 1/9 of overall dataset)

    americana_testing = df_americana.sample(frac=0.33)
    cornuta_testing = df_cornuta.sample(frac=0.33)

    testing = pd.concat([avellana_testing, americana_testing,
                         cornuta_testing])  # 1/3 of overall dataset with equal number of each class

    training = pd.concat([df, testing]).drop_duplicates(
        keep=False)  # 2/3 of overall dataset with equal number of each class

    training = training.sample(frac=1)  # randomise order of training dataset
    testing = testing.sample(frac=1)

    return training, testing


"""
    Writes to separate csv files the training and testing datasets for Weka import

    Parameters
    ----------
    training : Dataframe
        Training dataset.
    testing : Dataframe
        Testing dataset.
"""


def write_weka_file(training, testing, train_test_num):
    # must create an array of "avellana", "americana", "cornuta" strings corrsponding to classes from training dataframe
    training_classes_arr = []
    training_arr = training.values  # convert dataframe to ndarray
    training_classes_cols = training_arr[:, 10:13]  # get matrix of just the "avellana", "americana", "cornuta" columns
    for row in training_classes_cols:
        if (row == np.array([1, 0, 0])).all():
            training_classes_arr.append('avellana')
        if (row == np.array([0, 1, 0])).all():
            training_classes_arr.append('americana')
        if (row == np.array([0, 0, 1])).all():
            training_classes_arr.append('cornuta')
    new_training_df = training.drop(["avellana", "americana", "cornuta"], axis=1)  # dataframe without "avellana", "americana", "cornuta" columns
    new_training_df['variety'] = np.array(training_classes_arr).T  # create new column "variety" with column vector assigned

    # must create an array of "avellana", "americana", "cornuta" strings corrsponding to classes from testing dataframe
    testing_classes_arr = []
    testing_arr = testing.values
    testing_classes_cols = testing_arr[:, 10:13]
    for row in testing_classes_cols:
        if (row == np.array([1, 0, 0])).all():
            testing_classes_arr.append('avellana')
        if (row == np.array([0, 1, 0])).all():
            testing_classes_arr.append('americana')
        if (row == np.array([0, 0, 1])).all():
            testing_classes_arr.append('cornuta')
    new_testing_df = testing.drop(["avellana", "americana", "cornuta"], axis=1)
    new_testing_df['variety'] = np.array(testing_classes_arr).T

    # write to csv files the new dataframes
    new_training_df.to_csv(r"C:\Users\andrei\Desktop\AndreiLocal\NUIG\4thYr_70\ML\Assignment2\weka\training{0}.csv"
                           .format(train_test_num), index=None, header=True)
    new_testing_df.to_csv(r"C:\Users\andrei\Desktop\AndreiLocal\NUIG\4thYr_70\ML\Assignment2\weka\testing{0}.csv"
                          .format(train_test_num), index=None, header=True)


"""
    Plots the cost function evolving over epochs during training.

    Parameters
    ----------
    num_epochs : int
        Number of epochs in training
    costs : ndarray
       Array of cost function values for each epoch.
"""


def plot_cost(num_epochs, costs):
    iter_num = np.arange(num_epochs)
    plt.plot(iter_num, costs, '-ok', color='black')
    plt.xlabel("epochs")
    plt.ylabel("cost")
    plt.show()


"""
    Reads hazelnuts dataset from file.
    Sorts the dataset into classes.
    Takes 1/3 of each class (3 * 1/9 of total) and concatenate to form 1/3 dataset for testing.
    Assumes 3 classes with labels "avellana", "americana", "cornuta".
    Takes remaining 2/3 of dataset for training.
    

    Parameters
    ----------
    filepath : string
        Path to hazelnuts file.

    Returns
    -------
    training : Pandas Dataframe
        training dataset
        
    testing : Pandas Dataframe
        testing dataset   
"""


def preprocess(filepath):
    df = pd.read_excel(filepath)
    df_avellana = df.loc[df['avellana'] == 1]  # dataframe of avellana hazelnuts (approx. 1/3 of overall dataset)
    df_americana = df.loc[df['americana'] == 1]
    df_cornuta = df.loc[df['cornuta'] == 1]
    avellana_testing = df_avellana.sample(
        frac=0.33)  # random select 1/3 of avellana dataset (approx. 1/9 of overall dataset)
    americana_testing = df_americana.sample(frac=0.33)
    cornuta_testing = df_cornuta.sample(frac=0.33)
    testing = pd.concat([avellana_testing, americana_testing,
                         cornuta_testing])  # 1/3 of overall dataset with equal number of each class
    training = pd.concat([df, testing]).drop_duplicates(
        keep=False)  # 2/3 of overall dataset with equal number of each class
    return training, testing  # return dataframes


"""
    Randomly shuffles training and testing datasets.
    Forms numpy matrices from datasets:
        - training X, normalised (X training examples)
        - testing X, normalised with same parameters as training X (X testing examples)
        - training Y (Y ground truths corresponding to training X)
        - testing Y (Y ground truths corresponding to testing X)
        - 

    Parameters
    ----------
    training : Dataframe
        training dataset dataframe.
    testing : Dataframe
       training dataset dataframe.

    Returns
    -------
    training_X : matrix
        X dataset for training
        
    training_Y : matrix
        Corresponding Y ground truths dataset for training
    
    testing_X : matrix
        X dataset for testing
    
    testing_Y : matrix
        Corresponding Y ground truths dataset for testing

"""


def shuffle_datasets(training, testing):
    scaler = MinMaxScaler()

    training_df = training.sample(frac=1)  # randomise order of training dataset
    testing_df = testing.sample(frac=1)

    training_Xdf = training_df.iloc[0:, 0:10]  # X training dataframe (without corresponding Y values)
    training_Ydf = training_df.iloc[0:, 10:13]  # Y training dataframe for corresponding training X values

    testing_Xdf = testing_df.iloc[0:, 0:10]  # X testing dataframe
    testing_Ydf = testing_df.iloc[0:, 10:13]  # Y testing dataframe for corresponding testing X values

    training_X_un = np.asmatrix(training_Xdf.to_numpy())  # X unnormalised training matrix
    training_X_n = scaler.fit_transform(training_X_un)
    training_X = training_X_n.T  # X normalised training matrix

    training_Y = np.asmatrix(training_Ydf.to_numpy()).T  # Y training matrix

    testing_X_un = np.asmatrix(testing_Xdf.to_numpy())  # X unnormalised testing matrix
    testing_X_n = scaler.transform(testing_X_un)  # transform testing X with same fit as training X
    testing_X = testing_X_n.T  # X normalised testing matrix

    testing_Y = np.asmatrix(testing_Ydf.to_numpy()).T  # Y testing matrix

    return training_X, training_Y, testing_X, testing_Y


"""
    Creates confusion matrix for one test run

    Parameters
    ----------
    A2 : matrix
        Predictions made by MLP in probabilities form.
    testing_Y : matrix
       Ground truths of actual classes.
    threshold : int
       Probabilities above this value in A2 will be assigned 1, otherwise 0

    Returns
    -------
    conf_mx : array
        confusion matrix.
"""


def get_conf_mx(A2, testing_Y, threshold):
    y_hat_th = (A2 > threshold).astype(int)
    arr_y = []
    arr_y_hat = []

    for c in testing_Y.T:
        if (c == np.array([1, 0, 0])).all():
            arr_y.append(0)  # 0 = avellana
        if (c == np.array([0, 1, 0])).all():
            arr_y.append(1)  # 1 = americana
        if (c == np.array([0, 0, 1])).all():
            arr_y.append(2)  # 2 = cornuta
    for c in y_hat_th.T:
        if (c == np.array([1, 0, 0])).all():
            arr_y_hat.append(0)  # 0 = avellana
        if (c == np.array([0, 1, 0])).all():
            arr_y_hat.append(1)  # 1 = americana
        if (c == np.array([0, 0, 1])).all():
            arr_y_hat.append(2)  # 2 = cornuta
        if (c == np.array([0, 0, 0])).all():
            arr_y_hat.append(randrange(3))  # random assigned if uncertain for any class

    conf_mx = confusion_matrix(arr_y, arr_y_hat)
    return conf_mx


"""
    Trains MLP for a number of epochs specified.
    Plots cost function values over epochs.

    Parameters
    ----------
    mlp : MLP
        The untrained MLP.
    num_epochs : int
       Number of training epochs.
    training_X : matrix
       X dataset for training.
    training_Y : matrix
       Y ground truths dataset for training.

    Returns
    -------
    mlp : MLP
        Trained MLP.

"""


def train_mlp(mlp, num_epochs, training_X, training_Y):
    cost_array = []
    for i in range(0, num_epochs):
        Z1, A1, Z2, A2 = mlp.forward_prop(training_X)
        dW1, db1, dW2, db2 = mlp.back_prop(Z1, A1, A2, training_X, training_Y)
        mlp.update_weights(dW1, db1, dW2, db2)
        accuracy = calculate_accuracy(A2, training_Y, 0.5)
        cost_val = cost_function(A2, training_Y)
        print("Epoch: " + str(i) + " accuracy: " + str(accuracy) + "%")
        print("Cost function value: " + str(cost_val))
        cost_array.append(cost_val)
    # plot_cost(num_epochs, cost_array)
    return mlp  # returns the trained MLP


"""
    Tests MLP once.

    Parameters
    ----------
    mlp : MLP
        Trained MLP.
    testing_X : matrix
       X dataset for testing.
    testing_Y : matrix
       Y ground truths dataset for testing.
    threshold : int
       Any probabilites in matrix predicted by MLP above threshold will be 1, others 0

    Returns
    -------
    accuracy : int
        Number of correctly classified examples in testing_X.
    conf_mx  : array
        Confusion matrix of MLP results.

"""


def test_mlp(mlp, testing_X, testing_Y, threshold):
    Z1, A1, Z2, A2 = mlp.forward_prop(testing_X)
    accuracy = calculate_accuracy(A2, testing_Y, 0.5)
    conf_mx = get_conf_mx(A2, testing_Y, threshold)
    return accuracy, conf_mx


"""
    Trains MLP for a number of epochs, then tests it. Repeats for a number of cross validations.
    Writes results for each cross validation to a text file.

    Parameters
    ----------
    mlp : MLP
        Untrained MLP.
    num_cross_valids : int
       Number of cross validations for testing.
    num_epochs : int
       Number of epochs for training.
    threshold : int
       Any probabilites in matrix predicted by MLP above threshold will be 1, others 0.
    training : Dataframe
       Training dataframe that will be split into X matrix and Y matrix.
    testing : Dataframe
       Testing dataframe that will be split into X matrix and Y matrix.
"""


def train_test_mlp(mlp, num_cross_valids, num_epochs, threshold, training, testing):
    acc_arr = []
    conf_mx_arr = []
    for i in range(0, num_cross_valids):
        training_X, training_Y, testing_X, testing_Y = shuffle_datasets(training, testing)
        mlp_trained = train_mlp(mlp, num_epochs, training_X, training_Y)
        acc, conf_mx = test_mlp(mlp_trained, testing_X, testing_Y, threshold)
        acc_arr.append(acc)
        conf_mx_arr.append(conf_mx)
    avg_accuracy = np.average(acc_arr)
    conf_mx_np_arr = np.asarray(conf_mx_arr)
    write_conf_file(conf_mx_np_arr, acc_arr)
    print("Average accuracy after " + str(num_cross_valids) + " fold cross validation = " + str(avg_accuracy))


# ------ needed for both 10 fold c. v. & single train/test ------
training, testing = preprocess(r"C:\Users\andrei\Desktop\AndreiLocal\NUIG\4thYr_70\ML\Assignment2\hazelnuts.xlsx")
mlp_ut = MLP(0.1, 10, 6, 3)

# ------ 10 fold cross validation------
train_test_mlp(mlp_ut, 10, 1000, 0.5, training, testing)

# ------ single train and test ------
# training_X, training_Y, testing_X, testing_Y = shuffle_datasets(training, testing)
# mlp_t = train_mlp(mlp_ut, 5000, training_X, training_Y)
# test_mlp(mlp_t, testing_X, testing_Y, 0.5)

# ------ create training & testing files for Weka ------
#df = pd.read_excel(r"C:\Users\andrei\Desktop\AndreiLocal\NUIG\4thYr_70\ML\Assignment2\hazelnuts.xlsx")
#for i in range(10):
#    training_n, testing_n = preprocess_weka_datasets(df)
#    write_weka_file(training_n, testing_n, i)


